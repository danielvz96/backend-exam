from django.urls import reverse
from collections import Iterator, Generator
from rest_framework import status
from rest_framework.test import APITestCase
import sync_api_utils
import unittest

from .models import Match


class TestSyncApiUtils(unittest.TestCase):
    data = sync_api_utils.get_json_data()
    matchs_dict = sync_api_utils.get_keyword_from_dict(data, 'matchs')
    matchs_gen = sync_api_utils.matchs_generator_from_dict(matchs_dict)

    def testGetJsonDataType(self):
        self.assertIsInstance(self.data, dict)

    def testGetMatchsFromDictType(self):
        self.assertIsInstance(self.matchs_dict[0], dict)

    def testGetMatchsFromDictContent(self):
        self.assertIsInstance(self.matchs_dict[0], dict)

    def testMatchsGeneratorFromDictType(self):
        self.assertIsInstance(self.matchs_gen, Iterator)
        self.assertIsInstance(self.matchs_gen, Generator)


class MatchsTest(APITestCase):
    def test_sync_matchs(self):
        url = reverse('sync-matchs')
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


def main():
    unittest.main()


if __name__ == '__main__':
    main()