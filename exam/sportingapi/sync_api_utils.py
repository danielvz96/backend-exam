import json
import requests


def get_json_data(endpoint='http://sporting.backend.masfanatico.cl/api/match/in_competition/?competition=torneo-descentralizado&format=json'):
    """
    Hace un request al endpoint y retorna un diccionario.
    """
    request = requests.get(endpoint)
    data_dict = json.loads(request.text)
    return data_dict


def get_keyword_from_dict(data, keyword):
    """
    Retorna el valor de 'keyword' en data #Busca el key matchs en un diccionario recursivamente, y retorna su valor.
    """
    for key in data.keys():
        if key == keyword:
            return data[key]
        elif isinstance(data[key], dict):
            new_data = get_keyword_from_dict(data[key], keyword)
            return new_data
    return False


def matchs_generator_from_dict(matchs):
    for match in matchs:
        yield match
