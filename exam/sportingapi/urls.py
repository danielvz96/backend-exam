from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import MatchViewSet

router = DefaultRouter()
router.register(r'matchs', MatchViewSet, base_name='matchs')

urlpatterns = [
    path('', include(router.urls))
]
