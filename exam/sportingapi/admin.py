from django.contrib import admin
from .models import Team, Match, Stadium, Competition

models = [Team, Match, Stadium, Competition]

# Register your models here.
admin.site.register(models)
