from django.db import models
import datetime


class Team(models.Model):
    uuid = models.UUIDField(editable=True)
    name = models.CharField(max_length=80)
    # Algunas imagenes estan hosteadas en S3, por lo que guardaré el URL.
    image = models.URLField(null=True)
    color_scoreboard = models.CharField(default="", max_length=100)
    slug = models.SlugField(allow_unicode=True, unique=True)

    def update_team_from_dict(self, team_dict):
        """
        Recive un diccionario que representa a un team, lo actualiza,
        """
        uuid = team_dict['uuid']
        name = team_dict['name']
        image = team_dict['image']
        color_scoreboard = team_dict['color_scoreboard']
        slug = team_dict['slug']
        self.name, self.image, self.color_scoreboard, self.slug = name, image, color_scoreboard, slug

    def __str__(self):
        return self.slug


class Stadium(models.Model):
    uuid = models.UUIDField(editable=True)
    name = models.CharField(max_length=60)
    image = models.URLField(null=True)
    image_scoreboard = models.URLField(null=True)
    slug = models.SlugField(allow_unicode=True, unique=True)

    def update_stadium_from_dict(self, stadium_dict):
        """
        Recibe un diccionario representando a un stadium, lo actualiza.
        """
        uuid = stadium_dict['uuid']
        name = stadium_dict['name']
        image = stadium_dict['image']
        image_scoreboard = stadium_dict['image_scoreboard']
        slug = stadium_dict['slug']

        self.name, self.image, self.image_scoreboard, self.slug = name, image, image_scoreboard, slug

    def __str__(self):
        return self.slug


class Competition(models.Model):
    uuid = models.UUIDField(editable=True)
    code = models.CharField(default="", max_length=60)
    name = models.CharField(max_length=80)
    slug = models.SlugField(allow_unicode=True)
    id_opta = models.PositiveSmallIntegerField(default=0)
    created_at = models.DateTimeField(null=True)
    modified_at = models.DateTimeField(null=True)
    status = models.PositiveSmallIntegerField(null=True)
    sort = models.PositiveSmallIntegerField(default=0)

    def update_competition_from_dict(self, competition_dict):
        """
        Recibe un diccionario representando a un objeto Competition, lo actualiza.
        """
        uuid = competition_dict['uuid']
        code = competition_dict['code']
        name = competition_dict['name']
        slug = competition_dict['slug']
        id_opta = competition_dict['id_opta']
        # Reemplazo de 'Z' en string que strptime pueda parsearla
        created_time = competition_dict['created_at'].replace('Z', 'UTC')
        modified_time = competition_dict['modified_at'].replace('Z', 'UTC')
        created_at = datetime.datetime.strptime(created_time, '%Y-%m-%dT%H:%M:%S.%f%Z')
        modified_at = datetime.datetime.strptime(modified_time, '%Y-%m-%dT%H:%M:%S.%f%Z')
        status = competition_dict['status']
        sort = competition_dict['sort']

        self.code, self.name, self.slug, self.id_opta = code, name, slug, id_opta
        self.created_at, self.modified_at, self.status = created_at, modified_at, status
        self.sort = sort

    def __str__(self):
        return self.slug


class Match(models.Model):
    uuid = models.UUIDField(editable=True)
    local_team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='%(class)s_local', null=True)
    visit_team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='%(class)s_visit', null=True)
    referee = models.CharField(max_length=50, blank=True, null=True)
    local_goals = models.PositiveSmallIntegerField(default=0)
    visit_goals = models.PositiveSmallIntegerField(default=0)
    date = models.DateField(null=True)
    time = models.TimeField(null=True)
    minute = models.PositiveIntegerField(default=0)

    # Asumiré que estos 3 fields corresponden a urls que después las apps pueden utilizar.
    image_streaming = models.URLField(null=True)
    alineaciones = models.URLField(null=True)
    heatmap = models.URLField(null=True)

    stadium = models.ForeignKey(Stadium, on_delete=models.CASCADE, null=True)
    status = models.PositiveSmallIntegerField(default=0)
    id_opta = models.PositiveSmallIntegerField(default=0)
    slug = models.SlugField(allow_unicode=True)
    stage = models.CharField(max_length=50, null=True, blank=True)
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE, null=True)
    timestamp = models.PositiveIntegerField(default=0)
    weather = models.FloatField(default=0.0)
    city_reference_a = models.CharField(max_length=100, default="")
    city_reference_b = models.CharField(max_length=100, default="")

    def update_match_from_dict(self, match_dict):
        self.uuid = match_dict['uuid']

        self.local_team, created = Team.objects.get_or_create(uuid=match_dict['local_team']['uuid'])
        self.local_team.update_team_from_dict(match_dict['local_team'])
        self.local_team.save()

        self.visit_team, created = Team.objects.get_or_create(uuid=match_dict['visit_team']['uuid'])
        self.visit_team.update_team_from_dict(match_dict['visit_team'])
        self.visit_team.save()

        self.referee = match_dict['referee']
        self.local_goals = match_dict['local_goals']
        self.visit_goals = match_dict['visit_goals']
        self.date = datetime.datetime.strptime(match_dict['date'], '%Y-%m-%d')
        self.time = datetime.datetime.strptime(match_dict['time'], '%H:%M:%S').time()
        self.minute = int(match_dict['minute'])
        self.image_streaming = match_dict['image_streaming']
        self.alineaciones = match_dict['alineaciones']
        self.heatmap = match_dict['heatmap']

        self.stadium, created = Stadium.objects.get_or_create(uuid=match_dict['stadium']['uuid'])
        self.stadium.update_stadium_from_dict(match_dict['stadium'])
        self.stadium.save()

        self.status = int(match_dict['status'])
        self.id_opta = int(match_dict['id_opta'])
        self.slug = match_dict['slug']
        self.stage = match_dict['stage']

        self.competition, created = Competition.objects.get_or_create(uuid=match_dict['competition']['uuid'])
        self.competition.update_competition_from_dict(match_dict['competition'])
        self.competition.save()

        self.timestamp = int(match_dict['timestamp'])
        self.weather = int(match_dict['weather'])
        self.city_reference_a = match_dict['city_reference_a']
        self.city_reference_b = match_dict['city_reference_b']

    def __str__(self):
        return self.slug

