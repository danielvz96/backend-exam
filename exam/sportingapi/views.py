from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework.permissions import IsAdminUser, AllowAny
from rest_framework import viewsets, status
from rest_framework.response import Response
from .sync_api_utils import get_keyword_from_dict, get_json_data, matchs_generator_from_dict


from .serializers import MatchSerializer
from .models import Match


class MatchViewSet(viewsets.ViewSet):

    def get_permissions(self):
        if self.action == 'update':
            return (IsAdminUser(), )
        elif self.action == 'list':
            return (AllowAny(), )
        else:
            return (AllowAny(), )

    @method_decorator(cache_page(40))
    def list(self, request):
        matchs = Match.objects.all()
        serializer = MatchSerializer(matchs, many=True)
        return Response(serializer.data)

    def update(self, request, pk):
        data = get_json_data()
        match_dicts = get_keyword_from_dict(data, 'matchs')
        matchs = matchs_generator_from_dict(match_dicts)
        for match in matchs:
            instance, created = Match.objects.get_or_create(uuid=match['uuid'])
            instance.update_match_from_dict(match)
            instance.save()

        return Response(status=status.HTTP_200_OK)

