from rest_framework import serializers
from .models import Match, Team, Stadium, Competition


class CompetitionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Competition
        exclude = ('id',)


class StadiumSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stadium
        exclude = ('id',)


class TeamSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        exclude = ('id',)


class MatchSerializer(serializers.ModelSerializer):
    local_team = TeamSerializer()
    visit_team = TeamSerializer()
    stadium = StadiumSerializer()
    competition = CompetitionSerializer()

    class Meta:
        model = Match
        exclude = ('id',)
        depth = 2
