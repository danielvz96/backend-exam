from django.apps import AppConfig


class SportingapiConfig(AppConfig):
    name = 'sportingapi'
