## Funx Backend developer test 

This is a simple software test designed to know the way how you resolve a simple problem using Python/Django.

We are looking for candidates who really enjoy coding and try to make the most reausable software elements.

## The problem

Everyday we work designing and integrating differents kind of endpoints to be consumed by massively downloaded mobile apps.

In this case you must help our client Sporting Cristal to have a copy in their own backend to serve through API to users.

## Mission:
* Database model
* Import data with a synchronizer (script) from Sporting Cristal endpoint
* Expose matchs through API Rest

## Optional (may improving your evaluation)

* Use a container system
* Unit testing
* Consider concurrency

## Considerations
* Framework to be used: Django 2.x
* To do a public fork

## Sporting Cristal endpoint:

    http://sporting.backend.masfanatico.cl/api/match/in_competition/?competition=torneo-descentralizado&format=json


## Delivery instructions 

After than you finish your development you have to do a pull request in this repository. This one will be review for us and then it will be rejected.

Good Luck!

Thanks for your time!