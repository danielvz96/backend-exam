import requests
from time import sleep, time
import sqlite3

"""
Script simple para hacer un request al servidor local cada cierta cantidad de tiempo (refresh_rate), para que actualice
la base de datos. El view utiliza autentificacion por medio de tokens para evitar que sea usado o abusado por un usuario
que no sea administrador.
"""
conn = sqlite3.connect('exam/db.sqlite3')
c = conn.cursor()
c.execute('SELECT key FROM authtoken_token LIMIT 1')
TOKEN = c.fetchone()[0]
conn.close()
UPDATE_ENDPOINT = 'http://localhost:8000/sportingapi/matchs/update/'

headers = {
    'Authorization': 'Token {}'.format(TOKEN),
}

# Uso un minimo de 30 porque en el servidor de prueba, este request tarda un poco menos de 30 segundos.
refresh_rate = 30

while True:
    start = time()
    request = requests.request('PUT', UPDATE_ENDPOINT, headers=headers)
    request_time = time() - start
    sleep_time = refresh_rate-int(request_time)
    if sleep_time < 0:
        sleep_time = 5
    print('Tiempo procesando: {}\nTiempo para sleep(): {}'.format(int(request_time), sleep_time))
    sleep(sleep_time)
